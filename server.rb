require 'sinatra'
require 'json'

users = [{ name: "A", points: 0, created_at: Time.now(), invited_by: [] }]
invitations = []


def distribute_points user, users
  user[:invited_by].each_with_index do |user_name, i|
    u = users.find {|h| h[:name] == user_name }
    u[:points] += 1.00 / (2**i)
  end
end

def render_reponse users
  response = Hash.new
  users.each do |user|
    response[user[:name]] = user[:points] if user[:points] > 0
  end
  response.to_json
end

get '/' do
  content_type :json
  'Welcome to Reward System'
end

post '/upload' do
  content_type :json

  unless params[:file] &&
         (tmpfile = params[:file][:tempfile]) &&
         (name = params[:file][:filename])
    @error = "No file selected"
    return 422
  end

  File.foreach(tmpfile) do |line|
    puts "Processing Line: ", line
    parts = line.split(' ')
    date, time, user1, action, user2 = parts
    date_time = Time.parse(date + ' ' + time)

    if action == 'recommends'
      new_invitation = {}
      new_invitation[:sender] = user1
      new_invitation[:receiver] = user2
      new_invitation[:sent_at] = date_time

      invitations.push(new_invitation)
      invitations.sort_by! {|hash| hash['sent_at'] }.reverse
    elsif action == 'accepts'
      # skip if user already exists
      next unless users.select {|hash| hash[:name] == user1 }.empty?
      # find the invitations from already sorted array of invitations and remove it from array
      invitation = invitations.select {|inv| inv[:receiver] == user1 }.first
      # continue if invitation is nil
      next if invitation.nil?
      invited_by = users.select {|user| user[:name] == invitation[:sender]}.first
      # continue if invited by user is not present
      next if invited_by.nil?
      
      new_user   = { points: 0 }
      new_user[:name] = user1
      new_user[:created_at] = date_time
      new_user[:invited_by] = [invited_by[:name]] + invited_by[:invited_by]
      # add users in array
      users.push(new_user)

      distribute_points(new_user, users)
    else
      next
    end
  end
  return render_reponse(users)
end

error 422 do
  { error: "You have an error" }
end