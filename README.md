Decided to use Sinatra for it bein very lightweight and ease of creating web service to handle requests and serve them. It uses thin which is a very light but strong app server.

As we are going to process this it in memory I will be using the memory of the application to store the objects like users, invitations and points I am not going to use any DB for that. I took this decision to make this application as light as possible as otherwise I would have to include the connections with the DB as well as some kind of ORM to handle these well.

Theere are cons of not selecting the DB as we will have to keep everything in memory and it can fail for the larger files but for simplicity I have done this without any kind of ORM or database

User "A" is assumed to be already in the system so that its easier to divide the actions in two categories only `recommends` and `accepts`

checkout the code:
run `git clone git@bitbucket.org:Naveed-RoR/ruby_web_service.git`
run `cd ruby_web_service`
run `bundle install`
run `ruby server.rb` to start the server

Regarding Server:

run `curl -v -F “file=@/path/to/filename"  http://localhost:4567/upload`


